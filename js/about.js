$(function(){
    $('#contacto').on('show.bs.modal', function(e){
      console.log('el modal se esta mostrando');
      $('#BtnContacto').removeClass('btn-outline-success');
      $('#BtnContacto').addClass('btn-primary');
    });

    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('el modal se ha ocultado');
      $('#BtnContacto').removeClass('btn-primary');
      $('#BtnContacto').addClass('btn-outline-success');
    });

});