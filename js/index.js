$(function(){
        
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2500
    });
    $('#contacto').on('show.bs.modal', function(e){
      console.log('el modal se esta mostrando');
      $('#BtnContacto').removeClass('btn-outline-success');
      $('#BtnContacto').addClass('btn-primary');

    });
    $('#contacto').on('shown.bs.modal', function(e){
      console.log('el modal se ha mostrado');
    });        
    $('#contacto').on('hide.bs.modal', function(e){
      console.log('el modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('el modal se ha ocultado');
      $('#BtnContacto').removeClass('btn-primary');
      $('#BtnContacto').addClass('btn-outline-success');
    });

  });